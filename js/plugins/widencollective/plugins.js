/**
 * @file
 * Custom CKEditor plugin for Widen Collective module.
 */

/**
 * Registers the plugin within the editor.
 */
(function ($, Drupal, CKEDITOR) {
  'use strict';

  let editorId = null;
  const cancelListener = function () {
    editorId = null;
  };
  CKEDITOR.plugins.add('widencollective', {
    requires: ['iframedialog'],
    icons: 'widencollective',
    init: function (editor) {
      // Attach button to toolbar.
      editor.ui.addButton('Widencollective', {
        label: Drupal.t('Widen Collective'),
        command: 'widencollectiveDialog',
      });
      editor.addCommand('widencollectiveDialog', {
        exec: CKEDITOR.widen.imageDialog,
      });
    },
  });

  CKEDITOR.widen = CKEDITOR.widen || {
    imageDialog: function (editor) {
      // Fetch widen search UI url.
      $.get(Drupal.url('admin/widen/search_url'), function (data) {
        editorId = encodeURIComponent(editor.name);
        if (data.status_code == '200') {
          // Create new search dialog.
          editor.addCommand('widencollectiveDialog', new CKEDITOR.dialogCommand('widenSearchDialog'));

          // Create an iframe object inside dialog.
          CKEDITOR.dialog.addIframe('widenSearchDialog', Drupal.t('Widen Collective'), data.url, 1200, 640, null, {
            buttons: [CKEDITOR.dialog.cancelButton],
          });
          editor.execCommand('widencollectiveDialog');
        }
        else {
          let msg = Drupal.t('Cannot connect to widen collective server. Please try again.');
          if (data.error) {
            msg = msg + '\n' + data.error;
          }
          alert(msg);
        }
        CKEDITOR.dialog.getCurrent().on('cancel', cancelListener);
      });
    },
  };

  // Listen to widen collective postMessage.
  window.addEventListener('message', function (event) {
    if (event.data.items && Array.isArray(event.data.items)) {
      const embedCode = event.data.items[0].embed_code;
      if (embedCode && editorId) {
        const currentEditor = CKEDITOR.instances[editorId];
        if (currentEditor) {
          currentEditor.insertHtml(embedCode);
        }
        editorId = null;
      }
      // Close the dialog.
      CKEDITOR.dialog.getCurrent().hide();
    }
  });

})(jQuery, Drupal, CKEDITOR);
