<?php

namespace Drupal\widencollective\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\user\UserDataInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\widencollective\WidencollectiveSearchService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Widencollective controller for the widencollective module.
 */
class WidencollectiveSearchController extends ControllerBase {

  /**
   * The user data factory service.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * Creates an WidencollectiveSearchController object.
   *
   * @param \Drupal\user\UserDataInterface $user_data
   *   The user data factory.
   */
  public function __construct(UserDataInterface $user_data) {
    $this->userData = $user_data;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.data')
    );
  }

  /**
   * Request winden search url.
   *
   * @return json
   *   Returns a JSON feed.
   */
  public function getSearchUrl() {
    $widen_account = $this->userData->get('widencollective', $this->currentUser()->id(), 'account');

    if (!isset($widen_account['widen_token'])) {
      $widen_account['widen_token'] = FALSE;
    }

    $result = WidencollectiveSearchService::getSearchConnectorUiUrl($widen_account['widen_token']);
    return new JsonResponse($result);
  }

}
